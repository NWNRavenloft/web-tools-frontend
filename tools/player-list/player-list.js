function initPlayerList()
{
    var player_data = {};

    fetch(config.api_url + 'player-list')
    .then(function(response) {
        return response.json();
    })
    .then(function(json) {
        player_data = json;
        var datatable = webix.ui({
            view:"datatable",
            container: "data-container",
            columns:[
                { id:"player_name",    header:"Player",              width:200},
                { id:"character_name",   header:"Character",    width:200},
                { id:"area",    header:"Area",      width:200},
                { id:"public_key",   header:"Public Key",         width:110},
                { id:"ip_address",   header:"IP Address",         width:110}
            ],
            data: player_data
        });
    });
}